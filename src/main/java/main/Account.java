package main;

public class Account {

    private int id;
    private int balance;

    private static int firstFreeId = 1;

    public Account() {
        this.id = firstFreeId;
        firstFreeId++;
        this.balance = 0;
    }

    public int getId() {
        return id;
    }

    public int getBalance() {
        return balance;
    }

    public void setBalance(int balance) {
        this.balance = balance;
    }

}
